(function () {
    'use strict';

    angular.module('app.blockproducers')
        .controller('blockproducersCtrl', ['$scope', 'utilities', 'governanceService', blockproducersCtrl]);

    function blockproducersCtrl($scope, utilities, governanceService) {
        
        governanceService.getBlockproduceres(function (returnData) {
            $scope.active_blockproducers = returnData[0];
            $scope.standby_blockproducers = returnData[1];
        });

        utilities.columnsort($scope, "total_votes", "sortColumn", "sortClass", "reverse", "reverseclass", "column");
        utilities.columnsort($scope, "total_votes", "sortColumn2", "sortClass2", "reverse2", "reverseclass2", "column2");
    }
    
})();
