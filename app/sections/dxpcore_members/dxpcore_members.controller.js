(function () {
    'use strict';

    angular.module('app.dxpcore_members')
        .controller('dxpcoreCtrl', ['$scope', 'utilities', 'governanceService', dxpcoreCtrl]);

    function dxpcoreCtrl($scope, utilities, governanceService) {

        governanceService.getDxpcoreMembers(function (returnData) {
            $scope.active_dxpcore = returnData[0];
            $scope.standby_dxpcore = returnData[1];
        });

        utilities.columnsort($scope, "total_votes", "sortColumn", "sortClass", "reverse", "reverseclass", "column");
        utilities.columnsort($scope, "total_votes", "sortColumn2", "sortClass2", "reverse2", "reverseclass2", "column2");

    }
})();
