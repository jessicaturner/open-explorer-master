(function() {
    'use strict';

    angular.module('app').factory('governanceService', governanceService);
    governanceService.$inject = ['$http', 'appConfig', 'utilities', 'networkService'];

    function governanceService($http, appConfig, utilities, networkService) {

        return {
            getDxpcoreMembers: function(callback) {
                var active_dxpcore = [];
                var standby_dxpcore = [];
                var dxpcore = [];

                networkService.getHeader(function (returnData) {
                    var dxpcore_count = returnData.dxpcore_count;

                    $http.get(appConfig.urls.python_backend + "/dxpcore_members").then(function(response) {
                        var counter = 1;
                        angular.forEach(response.data, function(value, key) {
                            var parsed = {
                                id: value[0].id,
                                total_votes: utilities.formatBalance(value[0].total_votes, 5),
                                url: value[0].url,
                                dxpcore_member_account: value[0].dxpcore_member_account,
                                dxpcore_member_account_name: value[0].dxpcore_member_account_name,
                                counter: counter
                            };

                            if(counter <= dxpcore_count) {
                                active_dxpcore.push(parsed);
                            }
                            else {
                                standby_dxpcore.push(parsed);
                            }
                            counter++;
                        });
                    });
                    dxpcore[0] = active_dxpcore;
                    dxpcore[1] = standby_dxpcore;
                    callback(dxpcore);
                });
            },
            getBlockproduceres: function(callback) {
                var active_blockproducers = [];
                var standby_blockproducers = [];
                var blockproducers = [];

                networkService.getHeader(function (returnData) {
                    var blockproducer_count = returnData.blockproducer_count;

                    $http.get(appConfig.urls.python_backend + "/blockproducers").then(function(response) {
                        var counter = 1;
                        angular.forEach(response.data, function(value, key) {
                            var parsed = {
                                id: value.id,
                                last_aslot: value.last_aslot,
                                last_confirmed_block_num: value.last_confirmed_block_num,
                                pay_vb: value.pay_vb,
                                total_missed: value.total_missed,
                                total_votes: utilities.formatBalance(value.total_votes, 5),
                                url: value.url,
                                blockproducer_account: value.blockproducer_account,
                                blockproducer_account_name: value.blockproducer_account_name,
                                counter: counter
                            };

                            if(counter <= blockproducer_count) {
                                active_blockproducers.push(parsed);
                            }
                            else {
                                standby_blockproducers.push(parsed);
                            }
                            counter++;
                        });
                    });
                    blockproducers[0] = active_blockproducers;
                    blockproducers[1] = standby_blockproducers;
                    callback(blockproducers);
                });
            },
            getBenefactors: function(callback) {
                $http.get(appConfig.urls.python_backend + "/benefactors").then(function(response) {
                    var benefactors_current = [];
                    var benefactors_expired = [];
                    var benefactors = [];
                    for(var i = 0; i < response.data.length; i++) {
                        var now = new Date();
                        var start = new Date(response.data[i][0].work_begin_date);
                        var end = new Date(response.data[i][0].work_end_date);

                        var votes_for = utilities.formatBalance(response.data[i][0].total_votes_for, 5);
                        var daily_pay = utilities.formatBalance(response.data[i][0].daily_pay, 5);
                        var tclass = "";

                        var benefactor;

                        var have_url = 0;
                        if(response.data[i][0].url && response.data[i][0].url !== "http://") {
                            have_url = 1;
                        }

                        if(now > end) {
                            tclass = "danger";
                            benefactor = {
                                name: response.data[i][0].name,
                                daily_pay: daily_pay,
                                url: response.data[i][0].url,
                                have_url: have_url,
                                votes_for: votes_for,
                                votes_against: response.data[i][0].total_votes_against,
                                benefactor: response.data[i][0].benefactor_account,
                                start: start.toDateString(),
                                end: end.toDateString(),
                                id: response.data[i][0].id,
                                benefactor_name: response.data[i][0].benefactor_account_name,
                                tclass: tclass, perc: response.data[i][0].perc
                            };
                            benefactors_expired.push(benefactor);
                        }
                        else {
                            var voting_now = "";
                            if(now > start) {
                                if(response.data[i][0].perc >= 50 && response.data[i][0].perc < 100) {
                                    tclass = "warning";
                                }
                                else if(response.data[i][0].perc >= 100) {
                                    tclass = "success";
                                }
                            }
                            else {
                                tclass = "";
                                if(start > now) {
                                    voting_now = "VOTING NOW!";
                                }
                            }
                            benefactor = {
                                name: response.data[i][0].name,
                                daily_pay: daily_pay,
                                url: response.data[i][0].url,
                                have_url: have_url,
                                votes_for: votes_for,
                                votes_against: response.data[i][0].total_votes_against,
                                benefactor: response.data[i][0].benefactor_account,
                                start: start.toDateString(),
                                end: end.toDateString(),
                                id: response.data[i][0].id,
                                benefactor_name: response.data[i][0].benefactor_account_name,
                                tclass: tclass,
                                perc: response.data[i][0].perc,
                                voting_now: voting_now
                            };
                            benefactors_current.push(benefactor);
                        }
                    }
                    benefactors[0] = benefactors_current;
                    benefactors[1] = benefactors_expired;
                    callback(benefactors);
                });
            },
            getProxies: function(callback) {
                $http.get(appConfig.urls.python_backend + "/top_proxies").then(function(response) {
                    var proxies = [];
                    var counter = 1;
                    angular.forEach(response.data, function(value, key) {
                        var parsed = {
                            position: counter,
                            account: value.id,
                            account_name: value.name,
                            power: value.dxp_weight,
                            followers: value.followers,
                            perc: value.dxp_weight_percentage
                        };
                        if(counter <= 10) {
                            proxies.push(parsed);
                        }
                        counter++;
                    });
                    callback(proxies);
                });
            },
            getBlockproducerVotes: function(callback) {
                $http.get(appConfig.urls.python_backend + "/blockproducers_votes").then(function(response2) {
                    var blockproducers = [];
                    angular.forEach(response2.data, function (value, key) {
                        var parsed = {
                            id: value.benefactor_id,
                            blockproducer_account_name: value.benefactor_account_name,
                            proxy1: value[2].split(":")[1],
                            proxy2: value[3].split(":")[1],
                            proxy3: value[4].split(":")[1],
                            proxy4: value[5].split(":")[1],
                            proxy5: value[6].split(":")[1],
                            proxy6: value[7].split(":")[1],
                            proxy7: value[8].split(":")[1],
                            proxy8: value[9].split(":")[1],
                            proxy9: value[10].split(":")[1],
                            proxy10: value[11].split(":")[1],
                            tclass1: ((value[2].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass2: ((value[3].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass3: ((value[4].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass4: ((value[5].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass5: ((value[6].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass6: ((value[7].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass7: ((value[8].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass8: ((value[9].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass9: ((value[10].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass10: ((value[11].split(":")[1] === "Y") ? "success" : "danger")
                        };
                        blockproducers.push(parsed);
                    });
                    callback(blockproducers);
                });
            },
            getBenefactorsVotes: function(callback) {
                $http.get(appConfig.urls.python_backend + "/benefactors_votes").then(function(response2) {
                    var benefactors = [];
                    angular.forEach(response2.data, function (value, key) {
                        var parsed = {
                            id: value[1],
                            benefactor_account_name: value[0],
                            benefactor_name: value[2],
                            proxy1: value[3].split(":")[1],
                            proxy2: value[4].split(":")[1],
                            proxy3: value[5].split(":")[1],
                            proxy4: value[6].split(":")[1],
                            proxy5: value[7].split(":")[1],
                            proxy6: value[8].split(":")[1],
                            proxy7: value[9].split(":")[1],
                            proxy8: value[10].split(":")[1],
                            proxy9: value[11].split(":")[1],
                            proxy10: value[12].split(":")[1],
                            tclass1: ((value[3].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass2: ((value[4].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass3: ((value[5].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass4: ((value[6].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass5: ((value[7].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass6: ((value[8].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass7: ((value[9].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass8: ((value[10].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass9: ((value[11].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass10: ((value[12].split(":")[1] === "Y") ? "success" : "danger")
                        };
                        benefactors.push(parsed);
                    });
                    callback(benefactors);
                });
            },
            getDxpcoreVotes: function(callback) {
                $http.get(appConfig.urls.python_backend + "/dxpcore_votes").then(function(response) {
                    var dxpcore = [];
                    angular.forEach(response.data, function (value, key) {
                        var parsed = {
                            id: value[1],
                            dxpcore_account_name: value[0],
                            proxy1: value[1].split(":")[1],
                            proxy2: value[2].split(":")[1],
                            proxy3: value[3].split(":")[1],
                            proxy4: value[4].split(":")[1],
                            proxy5: value[5].split(":")[1],
                            proxy6: value[6].split(":")[1],
                            proxy7: value[7].split(":")[1],
                            proxy8: value[8].split(":")[1],
                            proxy9: value[9].split(":")[1],
                            proxy10: value[10].split(":")[1],
                            tclass1: ((value[1].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass2: ((value[2].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass3: ((value[3].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass4: ((value[4].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass5: ((value[5].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass6: ((value[6].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass7: ((value[7].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass8: ((value[8].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass9: ((value[9].split(":")[1] === "Y") ? "success" : "danger"),
                            tclass10: ((value[10].split(":")[1] === "Y") ? "success" : "danger")
                        };
                        dxpcore.push(parsed);
                    });
                    callback(dxpcore);
                });
            }
        };
    }

})();
