// Core asset
require("file-loader?name=asset-symbols/[name].png!./dxp.png");

// SmartTokens
require("file-loader?name=asset-symbols/[name].png!./aed.png");
require("file-loader?name=asset-symbols/[name].png!./usd.png");
require("file-loader?name=asset-symbols/[name].png!./aud.png");
require("file-loader?name=asset-symbols/[name].png!./cad.png");
require("file-loader?name=asset-symbols/[name].png!./chf.png");
require("file-loader?name=asset-symbols/[name].png!./eur.png");
require("file-loader?name=asset-symbols/[name].png!./cny.png");
require("file-loader?name=asset-symbols/[name].png!./gbp.png");
require("file-loader?name=asset-symbols/[name].png!./hkd.png");
require("file-loader?name=asset-symbols/[name].png!./idr.png");
require("file-loader?name=asset-symbols/[name].png!./inr.png");
require("file-loader?name=asset-symbols/[name].png!./jpy.png");
require("file-loader?name=asset-symbols/[name].png!./krw.png");
require("file-loader?name=asset-symbols/[name].png!./mop.png");
require("file-loader?name=asset-symbols/[name].png!./mxn.png");
require("file-loader?name=asset-symbols/[name].png!./nzd.png");
require("file-loader?name=asset-symbols/[name].png!./rub.png");
require("file-loader?name=asset-symbols/[name].png!./sgd.png");
require("file-loader?name=asset-symbols/[name].png!./sek.png");
require("file-loader?name=asset-symbols/[name].png!./star.png");
require("file-loader?name=asset-symbols/[name].png!./thb.png");
require("file-loader?name=asset-symbols/[name].png!./gold.png");
require("file-loader?name=asset-symbols/silver.png!./silver.png");

// 3rd party assets
require("file-loader?name=asset-symbols/[name].png!./ada.png");
require("file-loader?name=asset-symbols/[name].png!./advt.png");
require("file-loader?name=asset-symbols/[name].png!./annt.png");
require("file-loader?name=asset-symbols/[name].png!./bnb.png");
require("file-loader?name=asset-symbols/[name].png!./btc.png");
require("file-loader?name=asset-symbols/[name].png!./comp.png");
require("file-loader?name=asset-symbols/[name].png!./dot.png");
require("file-loader?name=asset-symbols/[name].png!./eos.png");
require("file-loader?name=asset-symbols/[name].png!./eth.png");
require("file-loader?name=asset-symbols/[name].png!./evnt.png");
require("file-loader?name=asset-symbols/[name].png!./grup.png");
require("file-loader?name=asset-symbols/[name].png!./jobs.png");
require("file-loader?name=asset-symbols/[name].png!./news.png");
//require("file-loader?name=asset-symbols/[name].png!./pnt.png");
require("file-loader?name=asset-symbols/[name].png!./poll.png");
require("file-loader?name=asset-symbols/[name].png!./proj.png");
require("file-loader?name=asset-symbols/[name].png!./reso.png");
require("file-loader?name=asset-symbols/[name].png!./sol.png");
require("file-loader?name=asset-symbols/[name].png!./srvc.png");
require("file-loader?name=asset-symbols/[name].png!./trx.png");
require("file-loader?name=asset-symbols/[name].png!./usdt.png");
require("file-loader?name=asset-symbols/[name].png!./xlm.png");
require("file-loader?name=asset-symbols/[name].png!./xrp.png");

require("file-loader?name=asset-symbols/[name].png!./steem.png");
require("file-loader?name=asset-symbols/[name].png!./mkr.png");
require("file-loader?name=asset-symbols/[name].png!./dgd.png");
require("file-loader?name=asset-symbols/[name].png!./obits.png");
require("file-loader?name=asset-symbols/[name].png!./dxpr.png");
require("file-loader?name=asset-symbols/[name].png!./dao.png");
require("file-loader?name=asset-symbols/[name].png!./lisk.png");
require("file-loader?name=asset-symbols/[name].png!./peerplays.png");
require("file-loader?name=asset-symbols/[name].png!./icoo.png");
require("file-loader?name=asset-symbols/[name].png!./blockpay.png");
require("file-loader?name=asset-symbols/[name].png!./dash.png");
require("file-loader?name=asset-symbols/[name].png!./eurt.png");
require("file-loader?name=asset-symbols/[name].png!./game.png");
require("file-loader?name=asset-symbols/[name].png!./grc.png");
require("file-loader?name=asset-symbols/[name].png!./bkt.png");
require("file-loader?name=asset-symbols/kapital.png!./bkt.png");
require("file-loader?name=asset-symbols/[name].png!./dct.png");
require("file-loader?name=asset-symbols/[name].png!./incnt.png");
require("file-loader?name=asset-symbols/[name].png!./nxc.png");
require("file-loader?name=asset-symbols/[name].png!./btwty.png");
require("file-loader?name=asset-symbols/[name].png!./open.btc.png");
require("file-loader?name=asset-symbols/[name].png!./hempsweet.png");
require("file-loader?name=asset-symbols/[name].png!./yoyow.png");
require("file-loader?name=asset-symbols/[name].png!./hero.png");
require("file-loader?name=asset-symbols/[name].png!./ruble.png");
require("file-loader?name=asset-symbols/[name].png!./oct.png");
require("file-loader?name=asset-symbols/[name].png!./smoke.png");
require("file-loader?name=asset-symbols/[name].png!./muse.png");
require("file-loader?name=asset-symbols/[name].png!./ppy.png");
require("file-loader?name=asset-symbols/[name].png!./stealth.png");
require("file-loader?name=asset-symbols/[name].png!./kexcoin.png");